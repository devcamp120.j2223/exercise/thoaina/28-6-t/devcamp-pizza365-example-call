import Button from "./components/Button";
import Text from "./components/Text";
import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  return (
    <div className="container">
      <Button />
      <Text />
    </div>
  );
}

export default App;
