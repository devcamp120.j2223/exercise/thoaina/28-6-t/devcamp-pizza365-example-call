import { Component } from "react";

class Button extends Component {
    render() {
        return (
            <div>
                <div class="form-group">
                    <p id="cmt2dev">Test Page for Javascrip Tasks. F5 to run code. </p>
                </div>
                <div class="form-group">
                    <form id="singleForm w-75">
                        <input type="button" class="btn btn-primary p-2" onclick="onBtnGetAllOrderClick()"
                            value="Call api get all orders!" />
                        <input type="button" class="btn btn-info p-2" onclick="onBtnCreateOrderClick()"
                            value="Call api create order!" />
                        <input type="button" class="btn btn-success p-2" onclick="onBtnGetOrderByIdClick()"
                            value="Call api get order by id!" />
                        <input type="button" class="btn btn-secondary p-2" onclick="onBtnUpdateOrderClick()"
                            value="Call api update order!" />
                        <input type="button" class="btn btn-danger p-2" onclick="onBtnCheckVoucherIdClick()"
                            value="Call api check voucher by id!" />
                        <input type="button" class="btn btn-success p-2" onclick="onBtnGetDrinkListClick()"
                            value="Call api Get drink list!" />
                    </form>
                </div>
            </div>
        )
    }
}

export default Button;