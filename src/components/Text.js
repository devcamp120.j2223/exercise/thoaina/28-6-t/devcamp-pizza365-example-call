import { Component } from "react";

class Text extends Component {
    render() {
        return (
            <div class="form-group">
                <p id="testP" style="font-size:larger;"> Demo 06 API for Pizza 365 Project: </p>
                <ul>
                    <li>get all Orders: lấy tất cả orders </li>
                    <li>create Order: tạo 1 order</li>
                    <li>get Order by ID: lấy 1 order bằng ID </li>
                    <li>update Order: update 01 order</li>
                    <li>check voucher by ID: check thông tin mã giảm giá, quan trọng là có hay không, và % giảm giá </li>
                    <li>get drink list: lấy danh sách đồ uống</li>
                </ul>
                <strong class="text-danger"> Bật console log để nhìn rõ output </strong>
            </div>
        )
    }
}

export default Text;